package com.sannedak.guesstheword.presentation.screens.score

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ScoreViewModel(private val finalScore: Int) : ViewModel() {

    private val _score = MutableLiveData(finalScore)

    val score: LiveData<Int>
        get() = _score
}
