package com.sannedak.guesstheword.presentation.screens.game

private val CORRECT_BUZZ_PATTERN = longArrayOf(100, 100, 100, 100, 100, 100)
private val PANIC_BUZZ_PATTERN = longArrayOf(0, 200)
private val GAME_OVER_BUZZ_PATTERN = longArrayOf(0, 2000)
private val NO_BUZZ_PATTERN = longArrayOf(0)

sealed class GameEvent<T> {

    val pattern: LongArray
        get() = when (this) {
            is BuzzCorrect -> CORRECT_BUZZ_PATTERN
            is BuzzPanic -> PANIC_BUZZ_PATTERN
            is BuzzGameOver -> GAME_OVER_BUZZ_PATTERN
            is NoBuzz -> NO_BUZZ_PATTERN
            else -> NO_BUZZ_PATTERN
        }
}

object BuzzCorrect : GameEvent<Any>()
object BuzzPanic: GameEvent<Any>()
object BuzzGameOver: GameEvent<Any>()
object NoBuzz: GameEvent<Any>()
object GameStarted: GameEvent<Any>()
object GameEnd: GameEvent<Any>()
