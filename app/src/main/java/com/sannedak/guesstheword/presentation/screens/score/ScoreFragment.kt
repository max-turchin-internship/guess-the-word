package com.sannedak.guesstheword.presentation.screens.score

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.ConstraintLayout
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.foundation.layout.ChainStyle
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.material.*
import com.sannedak.guesstheword.R
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.AmbientContext
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.ui.tooling.preview.Preview
import androidx.navigation.fragment.navArgs
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.sannedak.guesstheword.presentation.ui.*
import java.util.*

class ScoreFragment : Fragment() {

    private lateinit var scoreViewModel: ScoreViewModel
    private lateinit var viewModelFactory: ScoreViewModelFactory
    private lateinit var navController: NavController

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            GuessTheWordTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    ScoreScreen(scoreViewModel, navController)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val scoreFragmentArgs by navArgs<ScoreFragmentArgs>()
        viewModelFactory = ScoreViewModelFactory(scoreFragmentArgs.score)
        scoreViewModel = ViewModelProvider(this, viewModelFactory)
                .get(ScoreViewModel::class.java)
        navController = findNavController()
    }
}

@Composable
private fun ScoreScreen(scoreViewModel: ScoreViewModel, navController: NavController) {
    val finalScore = scoreViewModel.score.observeAsState(0)

    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
        val (youScoredText, scoreText, playAgainButton) = createRefs()
        createVerticalChain(youScoredText, scoreText, chainStyle = ChainStyle.Packed)

        val youScoredModifier = Modifier.constrainAs(youScoredText) {
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            top.linkTo(parent.top, bigPadding)
            bottom.linkTo(scoreText.top)
        }
        val scoreModifier = Modifier.constrainAs(scoreText) {
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            top.linkTo(youScoredText.bottom, padding)
            bottom.linkTo(playAgainButton.top, halfPadding)
        }
        val playAgainModifier = Modifier.constrainAs(playAgainButton) {
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            bottom.linkTo(parent.bottom, bigPadding)
        }

        Text(
                text = stringResource(id = R.string.you_scored),
                modifier = youScoredModifier,
                style = youScoredStyle
        )
        Text(text = "${finalScore.value}", modifier = scoreModifier, style = scoreEndStyle)
        Button(onClick = {
            val action = ScoreFragmentDirections.actionScoreFragmentToGameFragment()
            navController.navigate(action)
        }, modifier = playAgainModifier) {
            Text(text = stringResource(id = R.string.play_again).toUpperCase(Locale.getDefault()))
        }
    }
}

@Preview
@Composable
private fun ScoreScreenPreview() {
    val viewModel = ScoreViewModel(0)
    val navController = NavController(AmbientContext.current)

    GuessTheWordTheme {
        ScoreScreen(viewModel, navController)
    }
}
