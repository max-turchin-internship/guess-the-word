package com.sannedak.guesstheword.presentation.screens.game

import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.ChainStyle
import androidx.compose.foundation.layout.ConstraintLayout
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.core.content.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.sannedak.guesstheword.R
import androidx.ui.tooling.preview.Preview
import com.sannedak.guesstheword.data.repository.WordsMockRepository
import com.sannedak.guesstheword.presentation.BaseViewModelFactory
import com.sannedak.guesstheword.presentation.ui.*
import java.util.*

class GameFragment : Fragment() {

    private lateinit var navController: NavController

    private val repository = WordsMockRepository()
    private val gameViewModel: GameViewModel by viewModels {
        BaseViewModelFactory(this) { GameViewModel(repository) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            GuessTheWordTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    GameScreen(gameViewModel)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navController = findNavController()
        super.onViewCreated(view, savedInstanceState)
        gameViewModel.currentEvent.observe(
            viewLifecycleOwner,
            {
                if (it != GameStarted) {
                    if (it == GameEnd) {
                        val currentScore = gameViewModel.currentScore.value ?: 0
                        val action = GameFragmentDirections.actionGameFragmentToScoreFragment(currentScore)
                        navController.navigate(action)
                    } else {
                        buzz(it.pattern)
                    }
                }
            })
    }

    private fun buzz(pattern: LongArray) {
        val buzzer = activity?.getSystemService<Vibrator>()
        buzzer?.let { buzzer.vibrate(VibrationEffect.createWaveform(pattern, -1)) }
    }
}

@Composable
private fun GameScreen(gameViewModel: GameViewModel) {
    val currentTime = gameViewModel.currentTime.observeAsState("0:00")
    val currentScore = gameViewModel.currentScore.observeAsState(0)
    val currentWord = gameViewModel.currentWord.observeAsState("")

    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
        val (wordIsText, wordText, timerText, scoreText, skipButton, correctButton) = createRefs()
        createVerticalChain(wordIsText, wordText, chainStyle = ChainStyle.Packed)
        createHorizontalChain(skipButton, chainStyle = ChainStyle.SpreadInside)
        val line = createGuidelineFromBottom(guideLine)

        val wordIsModifier = Modifier.constrainAs(wordIsText) {
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            top.linkTo(parent.top)
            bottom.linkTo(wordText.top, padding)
        }
        val wordModifier = Modifier.constrainAs(wordText) {
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            top.linkTo(wordIsText.bottom)
            bottom.linkTo(scoreText.top)
        }
        val timerModifier = Modifier.constrainAs(timerText) {
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            bottom.linkTo(scoreText.top, halfPadding)
        }
        val scoreModifier = Modifier.constrainAs(scoreText) {
            start.linkTo(parent.start, halfPadding)
            end.linkTo(parent.end, halfPadding)
            bottom.linkTo(line)
        }
        val skipModifier = Modifier.constrainAs(skipButton) {
            start.linkTo(parent.start, padding)
            end.linkTo(correctButton.start)
            top.linkTo(line)
            bottom.linkTo(parent.bottom)
        }
        val correctModifier = Modifier.constrainAs(correctButton) {
            start.linkTo(skipButton.end)
            end.linkTo(parent.end, padding)
            top.linkTo(line)
            bottom.linkTo(parent.bottom)
        }

        Text(
            text = stringResource(id = R.string.word_is),
            modifier = wordIsModifier,
            style = wordIsStyle
        )
        Text(text = currentWord.value, modifier = wordModifier, style = wordStyle)
        Text(text = currentTime.value, modifier = timerModifier, style = timerStyle)
        Text(text = "Score: ${currentScore.value}", modifier = scoreModifier, style = scoreStyle)
        TextButton(onClick = { gameViewModel.onSkip() }, modifier = skipModifier) {
            Text(text = stringResource(id = R.string.skip).toUpperCase(Locale.getDefault()))
        }
        Button(onClick = { gameViewModel.onCorrect() }, modifier = correctModifier) {
            Text(text = stringResource(id = R.string.got_it).toUpperCase(Locale.getDefault()))
        }
    }
}

@Preview
@Composable
private fun GameScreenPreview() {
    val repository = WordsMockRepository()
    val gameViewModel = GameViewModel(repository)

    GuessTheWordTheme {
        GameScreen(gameViewModel)
    }
}
