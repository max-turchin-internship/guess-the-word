package com.sannedak.guesstheword.presentation.screens.title

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.ChainStyle
import androidx.compose.foundation.layout.ConstraintLayout
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.AmbientContext
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.ui.tooling.preview.Preview
import com.sannedak.guesstheword.R
import com.sannedak.guesstheword.presentation.ui.*
import java.util.*

class TitleFragment : Fragment() {

    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            GuessTheWordTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    TitleScreen(navController)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        navController = findNavController()
        super.onViewCreated(view, savedInstanceState)
    }
}

@Composable
private fun TitleScreen(navController: NavController) {
    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
        val (getReadyText, titleText, playButton) = createRefs()
        createVerticalChain(getReadyText, titleText, chainStyle = ChainStyle.Packed)

        val getReadyModifier = Modifier.constrainAs(getReadyText) {
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            top.linkTo(parent.top, halfPadding)
            bottom.linkTo(titleText.top, halfPadding)
        }
        val titleModifier = Modifier.constrainAs(titleText) {
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            top.linkTo(getReadyText.bottom, padding)
            bottom.linkTo(playButton.top)
        }
        val playButtonModifier = Modifier.constrainAs(playButton) {
            start.linkTo(parent.start)
            end.linkTo(parent.end)
            bottom.linkTo(parent.bottom, bigPadding)
        }

        Text(
            text = stringResource(id = R.string.get_ready),
            modifier = getReadyModifier,
            style = getReadyStyle
        )
        Text(
            text = stringResource(id = R.string.title_text),
            modifier = titleModifier,
            style = titleStyle
        )
        Button(
            onClick = {
                val action = TitleFragmentDirections.actionTitleFragmentToGameFragment()
                navController.navigate(action)
            },
            modifier = playButtonModifier
        ) { Text(text = stringResource(id = R.string.play_button).toUpperCase(Locale.getDefault())) }
    }
}

@Preview
@Composable
private fun TitleScreenPreview() {
    val navController = NavController(AmbientContext.current)

    GuessTheWordTheme {
        TitleScreen(navController)
    }
}
