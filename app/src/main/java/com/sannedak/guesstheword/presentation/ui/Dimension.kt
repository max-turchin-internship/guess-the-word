package com.sannedak.guesstheword.presentation.ui

import androidx.compose.ui.unit.dp

val halfPadding = 8.dp
val padding = 16.dp
val bigPadding = 32.dp
val guideLine = 96.dp