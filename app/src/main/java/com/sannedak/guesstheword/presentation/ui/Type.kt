package com.sannedak.guesstheword.presentation.ui

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import java.text.Normalizer

// Set of Material typography styles to start with
val typography = Typography(
    body1 = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    )
    /* Other default text styles to override
button = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.W500,
    fontSize = 14.sp
),
caption = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = 12.sp
)
*/
)

val getReadyStyle = TextStyle(
    fontSize = 14.sp,
    fontWeight = FontWeight.Normal,
    fontFamily = FontFamily.SansSerif
)
val titleStyle = TextStyle(
    fontSize = 34.sp,
    fontWeight = FontWeight.Normal,
    fontFamily = FontFamily.SansSerif
)
val wordIsStyle = TextStyle(
    fontSize = 14.sp,
    fontWeight = FontWeight.Normal,
    fontFamily = FontFamily.SansSerif
)
val wordStyle = TextStyle(
    fontSize = 34.sp,
    fontWeight = FontWeight.Normal,
    fontFamily = FontFamily.SansSerif
)
val timerStyle = TextStyle(
    fontSize = 14.sp,
    fontWeight = FontWeight.Normal,
    fontFamily = FontFamily.SansSerif
)
val scoreStyle = TextStyle(
    fontSize = 14.sp,
    fontWeight = FontWeight.Normal,
    fontFamily = FontFamily.SansSerif
)
val youScoredStyle = TextStyle(
    fontSize = 14.sp,
    fontWeight = FontWeight.Normal,
    fontFamily = FontFamily.SansSerif
)
val scoreEndStyle = TextStyle(
    fontSize = 34.sp,
    fontWeight = FontWeight.Normal,
    fontFamily = FontFamily.SansSerif
)
