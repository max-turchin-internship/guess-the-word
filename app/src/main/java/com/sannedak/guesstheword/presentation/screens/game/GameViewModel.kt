package com.sannedak.guesstheword.presentation.screens.game

import android.os.CountDownTimer
import android.text.format.DateUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.sannedak.guesstheword.data.repository.WordsRepository

class GameViewModel(private val repository: WordsRepository) : ViewModel() {

    companion object {
        // These represent different important times in the game, such as game length.

        // This is when the game is over
        private const val DONE = 0L

        // This is the time when the phone will start buzzing each second
        private const val COUNTDOWN_PANIC_SECONDS = 10L

        // This is the number of milliseconds in a second
        private const val ONE_SECOND = 1000L

        // This is the total time of the game
        private const val COUNTDOWN_TIME = 60000L
    }

    private lateinit var timer: CountDownTimer

    private lateinit var wordsList: MutableList<String>

    private var _currentEvent = MutableLiveData<GameEvent<Any>>(GameStarted)
    private val _currentTime = MutableLiveData<Long>()
    private val _currentScore = MutableLiveData(0)
    private val _currentWord = MutableLiveData<String>()

    private val currentTimeString = Transformations.map(_currentTime) { time ->
        DateUtils.formatElapsedTime(time)
    }

    val currentEvent: LiveData<GameEvent<Any>>
        get() = _currentEvent

    val currentTime: LiveData<String>
        get() = currentTimeString

    val currentScore: LiveData<Int>
        get() = _currentScore

    val currentWord: LiveData<String>
        get() = _currentWord

    init {
        getWords()
        startTimer()
    }

    fun onCorrect() {
        _currentEvent.value = BuzzCorrect
        _currentScore.value = _currentScore.value?.plus(1)
        nextWord()
    }

    fun onSkip() {
        _currentScore.value = _currentScore.value?.minus(1)
        nextWord()
    }

    private fun getWords() {
        wordsList = repository.getWordsList().toMutableList()
        wordsList.shuffle()
        nextWord()
    }

    private fun startTimer() {
        timer = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND) {
            override fun onTick(millisUntilFinished: Long) {
                _currentTime.value = (millisUntilFinished / ONE_SECOND)
                if (millisUntilFinished / ONE_SECOND <= COUNTDOWN_PANIC_SECONDS) {
                    _currentEvent.value = BuzzPanic
                }
            }

            override fun onFinish() {
                _currentTime.value = DONE
                _currentEvent.value = BuzzGameOver
                _currentEvent.value = GameEnd
            }
        }
        timer.start()
    }

    private fun nextWord() {
        if (wordsList.isEmpty()) {
            getWords()
        }
        _currentWord.value = wordsList.removeAt(0)
    }

    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }
}
