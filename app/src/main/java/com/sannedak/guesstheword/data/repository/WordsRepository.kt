package com.sannedak.guesstheword.data.repository

interface WordsRepository {

    fun getWordsList(): List<String>
}
