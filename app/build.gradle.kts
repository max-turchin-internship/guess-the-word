plugins {
    id(BuildOptions.androidApplication)
    id(BuildOptions.kotlinAndroid)
    id(BuildOptions.navigationSafeArgs)
}

android {
    compileSdkVersion(AndroidSdk.compile)

    defaultConfig {
        applicationId = Config.appId
        minSdkVersion(AndroidSdk.minimal)
        targetSdkVersion(AndroidSdk.target)
        versionCode(Config.code)
        versionName(Config.name)

        testInstrumentationRunner = Config.testRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
        useIR = true
    }

    buildFeatures {
        compose = true
        viewBinding = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = Compose.Versions.composeVersion
        kotlinCompilerVersion = Compose.Versions.kotlinCompiler
    }

    sourceSets {
        getByName("main").java.srcDirs("build/generated/source/navigation-args")
    }
}

dependencies {

    // Kotlin
    implementation(Kotlin.stdlib)

    // AndroidX
    implementation(AndroidX.core)
    implementation(AndroidX.appCompat)
    implementation(AndroidX.lifecycle.runtimeKtx)
    implementation(AndroidX.ui.tooling)
    implementation(AndroidX.fragmentKtx)

    // Navigation
    implementation(AndroidX.navigation.fragmentKtx)
    implementation(AndroidX.navigation.uiKtx)

    // Compose
    implementation(AndroidX.compose.ui)
    implementation(AndroidX.compose.runtime)
    implementation(AndroidX.compose.runtime.liveData)

    // Design
    implementation(Google.android.material)
    implementation(AndroidX.compose.material)
    implementation(AndroidX.compose.material.icons.extended)

    testImplementation(Testing.junit)
    androidTestImplementation(AndroidX.test.ext.junitKtx)
    androidTestImplementation(AndroidX.test.espresso.core)
}
